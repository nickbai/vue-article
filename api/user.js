import request from '@/utils/request'

export function loginByAuth () {
  return request({
    url: '/vue-api/user.php',
    method: 'post'
  })
}

export function checkLogin () {
  return request({
    url: '/vue-api/check.php',
    method: 'post'
  })
}
