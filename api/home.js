import axios from 'axios'

export function getHomeInfo () {
  return axios({
    url: process.env.BASE_API + '/vue-api/home.php',
    method: 'get'
  })
}

export function getHotNews () {
  return axios({
    url: process.env.BASE_API + '/vue-api/hot.php',
    method: 'get'
  })
}

export function getArticle (id) {
  return axios({
    url: process.env.BASE_API + '/vue-api/article.php',
    method: 'get',
    params: {id: id}
  })
}
