import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/views/home/index')
    },
    {
      path: '/list',
      name: 'list',
      component: () => import('@/views/hot/list')
    },
    {
      path: '/auth',
      name: 'auth',
      component: () => import('@/views/mine/auth')
    },
    {
      path: '/mine',
      name: 'mine',
      component: () => import('@/views/mine/index')
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: () => import('@/views/article/detail')
    }
  ]
})
