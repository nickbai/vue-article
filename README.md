# vue-article

> vue最简易实战项目，资讯系统。帮助您快速入门 vue

## 如何使用

``` bash
git clone https://gitee.com/nickbai/vue-article.git
cd vue-article
npm install
```
## 如何运行 

``` bash
npm run dev
```

## 如何部署接口 

本demo的接口部分是 php写的。项目目录下的 vue-api.zip 解压到服务器根目录。并且自己配置 
dev.env.js 下的 
```
BASE_API: '"http://localhost"'
```

## 先睹为快
![avatar](/screenshot/1.png) 
![avatar](/screenshot/2.png)
![avatar](/screenshot/3.png)
![avatar](/screenshot/4.png)
![avatar](/screenshot/5.png)
