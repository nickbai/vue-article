import { getToken, setToken } from '@/utils/auth'
import {loginByAuth, checkLogin} from '../../../api/user'

const user = {
  state: {
    token: getToken(),
    name: '',
    needLogin: true
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_LOGIN_STATUS: (state, status) => {
      state.needLogin = status
    }
  },

  actions: {
    LoginByAuth ({ commit }) {
      return new Promise((resolve, reject) => {
        loginByAuth().then(response => {
          const data = response.data
          if (data.code === 0) {
            commit('SET_TOKEN', data.data.token)
            commit('SET_NAME', data.data.name)
            commit('SET_LOGIN_STATUS', false)
            setToken(response.data.data.token)
          } else {
            commit('SET_LOGIN_STATUS', true)
          }
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    CheckLogin ({ commit }) {
      return new Promise((resolve, reject) => {
        checkLogin().then(response => {
          const data = response.data
          if (data.code === 0) {
            commit('SET_NAME', data.data.name)
            commit('SET_LOGIN_STATUS', false)
          } else {
            commit('SET_LOGIN_STATUS', true)
          }
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default user
