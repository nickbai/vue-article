const getters = {
  token: state => state.user.token,
  needLogin: state => state.user.needLogin,
  name: state => state.user.name
}

export default getters
